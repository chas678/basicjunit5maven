# README #

### What is this repository for? ###

* Basic project using Maven, Junit 5 and Java 8 (with Hamcrest)

## Console Launcher command

```bash
➜  basicjunit5maven git:(master) ✗ java -jar ~/java/junit5console/junit-platform-console-standalone-1.1.0.jar \
--scan-classpath ./target/test-classes  \
--class-path ./target/classes:./target/test-classes:\
/Users/chas/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar:\
/Users/chas/.m2/repository/org/hamcrest/hamcrest-library/1.3/hamcrest-library-1.3.jar
╷
├─ JUnit Jupiter ✔
│  └─ AppTest ✔
│     ├─ testApp() ✔
│     ├─ convertToIntBadParameterAssertThrows() ✔
│     ├─ assumeWindowsTest() ■ Assumption failed: assumption is not true
│     ├─ testWithStringParameter(String) ✔
│     │  ├─ run #1 with [Hello] ✔
│     │  └─ run #2 with [World] ✔
│     ├─ testWithMethodSourceStringParameter(String) ✔
│     │  ├─ run #1 with [foo] ✔
│     │  └─ run #2 with [bar] ✔
│     ├─ assertWithHamcrestMatcher() ✔
│     └─ myFirstTest() ✔
└─ JUnit Vintage ✔

Test run finished after 95 ms
[         5 containers found      ]
[         0 containers skipped    ]
[         5 containers started    ]
[         0 containers aborted    ]
[         5 containers successful ]
[         0 containers failed     ]
[         9 tests found           ]
[         0 tests skipped         ]
[         9 tests started         ]
[         1 tests aborted         ]
[         8 tests successful      ]
[         0 tests failed          ]

➜  basicjunit5maven git:(master) ✗
```
