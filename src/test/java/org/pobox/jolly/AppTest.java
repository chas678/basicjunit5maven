package org.pobox.jolly;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class AppTest {
    @Test
    public void convertToIntBadParameterAssertThrows() {
        NumberFormatException ex = assertThrows(NumberFormatException.class, () -> Integer.parseInt("TESTING!"));
        assertThat(ex.getMessage(), is("For input string: \"TESTING!\""));

    }

    @Test
    public void assumeWindowsTest() {
        assumeTrue(System.getProperty("os.name").startsWith("Windows"));
        assertTrue(true);
    }

    @Test
    void assertWithHamcrestMatcher() {
        assertThat(2 + 1, is(equalTo(3)));
    }

    @Test
    void myFirstTest() {
        assertEquals(2, 1 + 1);
    }

    @Test
    public void testApp() {
        assertTrue(true);
    }

    @ParameterizedTest(name = "run #{index} with [{arguments}]")
    @ValueSource(strings = {"Hello", "World"})
    void testWithStringParameter(String argument) {
        assertNotNull(argument);
    }

    @ParameterizedTest(name = "run #{index} with [{arguments}]")
    @MethodSource("myParams")
    void testWithMethodSourceStringParameter(String argument) {
        assertNotNull(argument);
    }

    static Stream<Arguments> myParams() {
        return Stream.of(
                Arguments.of("foo"),
                Arguments.of("bar")
        );
    }
}
